import email
import smtplib
from email.message import EmailMessage

email = EmailMessage()
email['from'] = 'Jalen'
email['to'] = ''
email['subject'] = 'testing python email'

email.set_content('I am testing emailing in python')

with smtplib.SMTP(host='smtp.outlook.com', port=587) as smtp:
    smtp.ehlo()
    smtp.starttls()
    smtp.login()
    smtp.send_message(email)
    print('all good')